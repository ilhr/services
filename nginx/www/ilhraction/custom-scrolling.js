let currentIndex = 0;

function scrollRightHandler() {
    const container = document.getElementById('scrollContainer');
    const groupWidth = document.querySelector('.text-box-group').offsetWidth; // Width of each group
    const totalGroups = container.children.length;
    if (currentIndex < totalGroups - 1) {
        currentIndex++;
        container.style.transform = `translateX(-${currentIndex * groupWidth}px)`;
    }
}

function scrollLeftHandler() {
    const container = document.getElementById('scrollContainer');
    const groupWidth = document.querySelector('.text-box-group').offsetWidth; // Width of each group
    if (currentIndex > 0) {
        currentIndex--;
        container.style.transform = `translateX(-${currentIndex * groupWidth}px)`;
    }
}