# Visual Hierarchy
In order to make an effective page layout, we need to establish guidelines for how elements should be positioned within the layout. 

## Fonts
There is only one font for the entire website, its called "Inter" and we choose the version sans-seriff. 
There are two layers of text on desktop and mobile:
1. Header: Font is bold, all capitalized
    * Mobile: 24 px
    * Desktop: 60 px
2. Subheader: Font is bold, all capitalized
    * Mobile: 16 px
    * Desktop: 24 px
2. Content: Font is normal, not capitalized
    * Mobile: 16 px
    * Desktop: 24 px

