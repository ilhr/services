#!/bin/sh

# mv /run/secrets/htpasswd /etc/nginx/.htpasswd

envsubst '${ENV_SUBDOMAIN}' < /etc/nginx/nginx.conf.template > /etc/nginx/nginx.conf

exec nginx -g 'daemon off;'
