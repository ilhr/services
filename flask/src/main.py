from openai import OpenAI
from flask import Flask, request, jsonify
from typing import Callable


app = Flask(__name__)

with open('/run/secrets/openai-api-key', 'r') as file:
    openai_api_key = file.read()

client = OpenAI(api_key=openai_api_key)

system_message = {
    "role": "system", 
    "content": 
        '''You are an AI with a deep specialization in United States immigration law. 
        You assist and support 'ILHR', a 501(c)(3) tax-exempt, non-profit immigration law 
        firm advancing immigration law as a human right. You provide users with accurate, 
        clear, and simplified information about immigration processes, forms, motions, 
        requirements, and basic legal advice based on USCIS, EOIR, Embassies, and other data. 
        Request necessary details like visa status, duration of stay, and immigration 
        objectives to provide relevant advice. Remind users to contact ILHR for 
        personalized legal advice.'''
}

models: dict[str, Callable[[list[dict[str, str]]], str]] = {}

def model(func: Callable[[list[dict[str, str]]], str]) -> None:
    models[func.__name__] = func

@model
def foo(messages: list[dict[str, str]]) -> str:
    return 'foo'

@model
def gpt_3_5_turbo(messages: list[dict[str, str]]) -> str:
    response = client.chat.completions.create(
        model='gpt-3.5-turbo',
        messages=[
            system_message,
            *messages
        ]
    )

    return response.choices[0].message.content


@app.route("/api/legal-chatbot/", methods=['POST'])
def legal_chatbot():
    payload = request.get_json()

    model = payload['model']

    messages = payload['messages']

    response = models[model](messages)

    return response